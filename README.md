# MavenDependTemplate

#### 介绍
多maven项目启动顺序 


##### 问题背景

在公司实际项目中，开发的项目一般都会涉及到多个 maven 项目，比如项目2会依赖项目1。使用 Jenkins 实现自动部署时，除了写一个流水线脚本时，就需要找一个合适的 maven 命令脚本解决。

##### 问题描述

现在有两个 Maven 工程，maven1 和 maven2 ，maven2 依赖于 maven1 。maven1 的 pom 文件如下：

```

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 
   http://maven.apache.org/xsd/maven-4.0.0.xsd"> 
   
   <modelVersion>4.0.0</modelVersion> 
   <groupId>bus-core-api</groupId> 
   <artifactId>bus-core-api</artifactId> 
   <version>1.0-SNAPSHOT</version> 
   <packaging>jar</packaging> 
   </project>
   
 ```
maven2 的pom文件如下：

```

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 
   http://maven.apache.org/xsd/maven-4.0.0.xsd"> 
   <modelVersion>4.0.0</modelVersion> 
   <groupId>app-desktop-ui</groupId> 
   <artifactId>app-desktop-ui</artifactId> 
   <version>1.0</version> 
   <packaging>jar</packaging> 
   <dependencies> 
    <dependency> 
        <groupId>bus-core-api</groupId> 
        <artifactId>bus-core-api</artifactId> 
        <version>1.0-SNAPSHOT</version> 
    </dependency> 
  </dependencies> 
  </project>

```

**在平时我们打包的时候，是对maven1项目先执行 maven install ，然后再对maven2 项目执行 maven clean package。
有没有一种方法可以实现：我直接对maven2进行打包操作，它会自动的先对maven2依赖的项目先进行install，然后再对maven2进行打包？**

##### 解决方案
###### 方案一

首先有两个 maven 项目，maven1 和 maven2 。其中 maven2 依赖于 maven1.

这时，maven1 的 pom.xml 内容如下：

```
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>space.tiantsing</groupId>
    <artifactId>maven1</artifactId>
    <version>1.0-SNAPSHOT</version>
    <packaging>jar</packaging>

</project>

```

maven2 的 pom.xml 内容如下：

```
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>space.tiantsing</groupId>
    <artifactId>maven2</artifactId>
    <version>1.0-SNAPSHOT</version>
    <packaging>jar</packaging>

    <dependencies>
        <dependency>
            <groupId>space.tiantsing</groupId>
            <artifactId>maven1</artifactId>
            <version>1.0-SNAPSHOT</version>
        </dependency>
    </dependencies>

</project>

```

然后，我们在 maven2 项目中新建一个 xml 文件，pomAutom.xml 内容如下：

```
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>space.tiantsing</groupId>
    <artifactId>mavenAutom</artifactId>
    <version>1.0-SNAPSHOT</version>
    <packaging>pom</packaging> // 这里的打包方式必须为 pom

    <modules>
        <module>../maven1</module>
        <module>../maven2</module>
    </modules>
</project>

```

这时，我们通过 cmd 进入到 maven2 项目 pom 文件所在的目录，执行命令 `mvn clean package -f pomAutom.xml` ,这时就会按照你在 pomAutom.xml 中

```
<modules>
        <module>../maven1</module>
        <module>../maven2</module>
    </modules>
    
```

配置的顺序启动。

DEMO路径：[MavenDependTemplate](https://gitee.com/tsingtian/MavenDependTemplate.git)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
